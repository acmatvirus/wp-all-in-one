<?php

if (!function_exists('render')) {
    function render($filename, $data)
    {
        ob_start();
        $data = $data;
        include_once WPAI_DERICTORY . "/application/helper/data_helper.php";
        require WPAI_DERICTORY . '/application/view/' . $filename;
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }
}

if (!function_exists('curl')) {
    function curl($url, $data, $method = 'GET')
    {
        // Khởi tạo một tài liệu cURL
        $ch = curl_init();
        // Thiết lập các tùy chọn của yêu cầu cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'CURL Error: ' . curl_error($ch);
        }
        curl_close($ch);
        return $response;
    }
}
