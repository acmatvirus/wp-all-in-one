<h2><?= WPAI_TITLE ?> Settings</h2>
<!--  -->
<ul id="tabs">
  <?php if (!empty($data['data']->contact_active)) : ?>
    <li data-tab="#tab-contact">Nút liên hệ</li>
  <?php endif; ?>
  <?php if (!empty($data['data']->review_active)) : ?>
    <li data-tab="#tab-review">Bình luận</li>
  <?php endif; ?>
  <?php if (!empty($data['data']->sitemap_active)) : ?>
    <li data-tab="#tab-sitemap">Sitemap</li>
  <?php endif; ?>
  <?php if (!empty($data['data']->optimize_active)) : ?>
    <li data-tab="#tab-optimize">Tối ưu hình ảnh</li>
  <?php endif; ?>
  <li data-tab="#tab-setting" class="current">Cài đặt</li>
</ul>
<!--  -->
<div class="tab-content" id="tab-contact">
  <form action="" method="post">
    <table>
      <thead>
        <tr style="text-align: center;">
          <td>Loại liên hệ</td>
          <td>Địa chỉ</td>
          <td>Tiêu đề</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Hotline</td>
          <td><input type="text" name="hotline[url]" value="<?= (!empty($data) && !empty($data['data']->hotline)) ? $data['data']->hotline->url : ''; ?>"></td>
          <td><input type="text" name="hotline[title]" value="<?= (!empty($data) && !empty($data['data']->hotline)) ? $data['data']->hotline->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Zalo</td>
          <td><input type="text" name="zalo[url]" value="<?= (!empty($data) && !empty($data['data']->zalo)) ? $data['data']->zalo->url : ''; ?>"></td>
          <td><input type="text" name="zalo[title]" value="<?= (!empty($data) && !empty($data['data']->zalo)) ? $data['data']->zalo->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Telegram</td>
          <td><input type="text" name="telegram[url]" value="<?= (!empty($data) && !empty($data['data']->telegram)) ? $data['data']->telegram->url : ''; ?>"></td>
          <td><input type="text" name="telegram[title]" value="<?= (!empty($data) && !empty($data['data']->telegram)) ? $data['data']->telegram->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Instagram</td>
          <td><input type="text" name="instagram[url]" value="<?= (!empty($data) && !empty($data['data']->instagram)) ? $data['data']->instagram->url : ''; ?>"></td>
          <td><input type="text" name="instagram[title]" value="<?= (!empty($data) && !empty($data['data']->instagram)) ? $data['data']->instagram->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Message</td>
          <td><input type="text" name="message[url]" value="<?= (!empty($data) && !empty($data['data']->message)) ? $data['data']->message->url : ''; ?>"></td>
          <td><input type="text" name="message[title]" value="<?= (!empty($data) && !empty($data['data']->message)) ? $data['data']->message->title : ''; ?>"></td>
        </tr>
        <tr>
          <td>Fanpage</td>
          <td><input type="text" name="fanpage[url]" value="<?= (!empty($data) && !empty($data['data']->fanpage)) ? $data['data']->fanpage->url : ''; ?>"></td>
          <td><input type="text" name="fanpage[title]" value="<?= (!empty($data) && !empty($data['data']->fanpage)) ? $data['data']->fanpage->title : ''; ?>"></td>
        </tr>
      </tbody>
    </table>
    <button name="bcontact_settings" value="save" class="button-primary">Save</button>
  </form>
</div>
<div class="tab-content" id="tab-review">

</div>
<div class="tab-content" id="tab-sitemap">

</div>
<div class="tab-content" id="tab-optimize">

</div>
<div class="tab-content current" id="tab-setting">
  <div class="wrap">
    <form action="" method="post">
      <?php if (empty($data['data']->key)) : ?>
        <input type="hidden" name="domain" value="<?= WPAIO_BASE_URL; ?>">
        <button name="type" value="generate" class="button-primary">Yêu cầu tạo key</button>
      <?php endif; ?>
      <?php if (!empty($data['data']->key)) : ?>
        <p>API KEY: <?= !empty($data['data']->key) ? $data['data']->key : 'No key'; ?></p>
        <hr>
        <h3>Chức năng</h3>
        <p><input type="checkbox" name="contact_active" value="1" <?= !empty($data['data']->contact_active) ? 'checked' : ''; ?>> Nút liên hệ</p>
        <p><input type="checkbox" name="review_active" value="1" <?= !empty($data['data']->review_active) ? 'checked' : ''; ?>> Bình luận</p>
        <p><input type="checkbox" name="sitemap_active" value="1" <?= !empty($data['data']->sitemap_active) ? 'checked' : ''; ?>> Sitemap</p>
        <p><input type="checkbox" name="optimize_active" value="1" <?= !empty($data['data']->optimize_active) ? 'checked' : ''; ?>> Tối ưu hình ảnh</p>
        <button name="save_settings" value="1" class="button-primary">Lưu cài đặt</button>
        <hr>
        <p></p>
      <?php endif; ?>
    </form>
  </div>
</div>
<script src="<?= WPAI_URL ?>public/js/admin.js"></script>