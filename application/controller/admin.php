<?php

add_action('admin_menu', function () {
    add_options_page(WPAI_TITLE, WPAI_TITLE, 'manage_options', WPAI_SLUG, function () {
        $data = json_decode(get_option('setting_' . WPAI_SLUG), true);
        if (empty($data)) $data = [];
        if (isset($_GET['update']) && $_GET['update'] == true) {
            $config = json_decode(file_get_contents("https://gitlab.com/acmatvirus/wp-all-in-one/-/raw/main/package.json"));
            $zip_url = 'https://gitlab.com/acmatvirus/wp-all-in-one/-/archive/main/wp-all-in-one-main.zip';
            $zip_content = file_get_contents($zip_url);
            if ($zip_content === false) {
                $message = '<div class="notice notice-success is-dismissible"><p>Error: cannot download zip file</p></div>';
            };
            $tmp_zip_path = '../wp-content/plugins/wp-all-in-one-main.zip';
            file_put_contents($tmp_zip_path, $zip_content);
            $zip = new ZipArchive;
            if ($zip->open($tmp_zip_path) === true) {
                $zip->extractTo("../wp-content/plugins/");
                $zip->close();
                shell_exec("service php-fpm reload");
                $message = '<div class="notice notice-success is-dismissible"><p>Update new version ' . $config->version . ' success</p></div>';
            } else {
                $message = '<div class="notice notice-success is-dismissible"><p>Error: cannot open zip file</p></div>';
            }
            unlink($tmp_zip_path);
        }

        if (isset($_POST['type']) && $_POST['type'] == 'generate' && empty($data['key'])) {
            $result = curl('https://socketapi.site/api/plugin/Optimizeimage/generate', ['domain' => WPAIO_BASE_URL], 'POST');
            if (!empty($result) && preg_match('/^[a-f0-9]{32}$/', $result)) {
                $data['key'] = $result;
                update_option('setting_' . WPAI_SLUG, json_encode($data));
                $message = '<div class="notice notice-success is-dismissible"><p>Tạo key thành công.</p></div>';
            } else {
                $message = '<div class="notice notice-error is-dismissible"><p>Có lỗi xảy ra.</p></div>';
            }
        };

        if (isset($_POST['save_settings'])) {
            unset($_POST['save_settings']);
            if (empty($_POST['contact_active']) && !empty($data['contact_active'])) unset($data['contact_active']);
            if (empty($_POST['review_active']) && !empty($data['review_active'])) unset($data['review_active']);
            if (empty($_POST['sitemap_active']) && !empty($data['sitemap_active'])) unset($data['sitemap_active']);
            if (empty($_POST['optimize_active']) && !empty($data['optimize_active'])) unset($data['optimize_active']);
            $data = json_encode(array_merge($data, $_POST));
            update_option('setting_' . WPAI_SLUG, $data);
            $message = '<div class="notice notice-success is-dismissible"><p>Cấu hình đã được lưu.</p></div>';
        };
        if (!empty($message)) echo $message;
        $data = json_decode(get_option('setting_' . WPAI_SLUG));
        $render = render('admin/index.php', [
            'data' => $data
        ]);
        echo $render;
    });
});
