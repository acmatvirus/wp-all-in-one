<?php
// INCLUDE HELPER
foreach (glob(WPAI_DERICTORY . 'application/helper/*.php') as $file) {
  include_once $file;
}
// INCLUDE CONTROLLER
foreach (glob(WPAI_DERICTORY . 'application/controller/*.php') as $file) {
  include_once $file;
}
// Enqueue styles for admin
add_action('admin_enqueue_scripts', function () {
  wp_enqueue_style(WPAI_SLUG . 'admin-style', WPAI_URL . 'public/css/admin.css', [], WPAI_ASSET_VERSION);
});
// Add link plugin
add_filter(WPAI_SETTINGLINK, function (array $links) {
  $url = get_admin_url() . "options-general.php?page=" . WPAI_SLUG;
  $settings_link = '<a href="' . $url . '">Settings</a>';
  $links[] = $settings_link;
  return $links;
});

if ($_SERVER['REQUEST_URI'] == '/wp-admin/plugins.php') {
  $config = json_decode(file_get_contents("https://gitlab.com/acmatvirus/wp-all-in-one/-/raw/main/package.json"));
  if (WPAI_VERSION !== $config->version) {
    add_filter(WPAI_UPDATELINK, function (array $links) {
      $url = get_admin_url() . "options-general.php?page=" . WPAI_SLUG . "&update=true";
      $settings_link = '<a href="' . $url . '">Update now</a>';
      $links[] = $settings_link;
      return $links;
    });
  }
}
