<?php if (!defined('WPINC')) die;
/* 
Plugin Name: 	Wp All in one
Plugin URI: 	https://acmatvirus.top
Description: 	This plugin Wp All in one by AcmaTvirus.
Tags: 			Acmatvirus, Plugin Wp All in one
Author: 		Acmatvirus
Author URI: 	https://acmatvirus.top
Version: 		1.0.00
License: 		GPL2
Text Domain:    Acmatvirus
*/
// ==VARIABLE
$config = json_decode(file_get_contents(plugin_dir_path(__FILE__) . '/package.json'));
// ==DEFINE
define('WPAIO_BASE_URL', get_option('siteurl'));
define('WPAI_URL', plugin_dir_url(__FILE__));
define('WPAI_DERICTORY', plugin_dir_path(__FILE__));
define('WPAI_SETTINGLINK', 'plugin_action_links_' . plugin_basename(__FILE__));
define('WPAI_UPDATELINK', 'plugin_action_links_' . plugin_basename(__FILE__));
define('WPAI_TITLE', $config->name);
define('WPAI_SLUG', $config->slug);
define('WPAI_VERSION', $config->version);
define('WPAI_ASSET_VERSION', $config->asset);
// ==INCLUDE CORE
include_once "system/core.php";
